export const PORT = 4567;
export const DATABASE_URI = 'mongodb://localhost:27017';
export const DATABASE_NAME = 'flashcards-server';
