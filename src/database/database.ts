import {Collection, Db, MongoClient} from 'mongodb';

import {DATABASE_NAME, DATABASE_URI} from '../config';

export class Database {
	private static db: Db;

	public static async initialise() {
		const client = await MongoClient.connect(DATABASE_URI, {
			useNewUrlParser: true,
		});
		console.log('Connected to mongodb server');
		this.db = client.db(DATABASE_NAME);
	}

	public static get cards(): Collection<any> {
		return this.db.collection('cards');
	}
}
