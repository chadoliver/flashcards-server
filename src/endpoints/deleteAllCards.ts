import express from 'express';
import {Database} from '../database/database';

export async function deleteAllCards() {
	const {deletedCount} = await Database.cards.deleteMany({});
	return {
		deletedCount,
	};
}
