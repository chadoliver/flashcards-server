import express from 'express';
import {Database} from '../database/database';

export async function getAllCards() {
	const cards = await Database.cards.find({}).toArray();
	return cards.map(({id, front, back}) => {
		return {
			id,
			front,
			back,
		};
	});
}
