import express from 'express';
import {Database} from '../database/database';

export async function getCardById(req: express.Request) {
	const id = req.params.id;
	const {front, back} = await Database.cards.findOne({id});
	return {
		id,
		front,
		back,
	};
}
