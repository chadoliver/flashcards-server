import express from 'express';
import {Database} from '../database/database';
import {getAllCards} from './getAllCards';

export async function getNextCard(req: express.Request, res: express.Response) {
	const allCards = await getAllCards();
	if (allCards.length < 1) {
		throw new Error('No cards');
	} else {
		const randomIndex = Math.floor(Math.random() * allCards.length);
		return allCards[randomIndex];
	}
}
