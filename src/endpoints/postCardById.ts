import express from 'express';
import {Database} from '../database/database';

export async function postCardById(req: express.Request) {
	const {id, front, back} = req.body;
	await Database.cards.insertOne({
		id,
		front,
		back,
	});
}
