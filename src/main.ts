import express from 'express';

import {PORT} from './config';
import {Database} from './database/database';
import {gracefullyHandleErrors} from './middleware/GracefullyHandleErrors';
import {logIncomingRequest} from './middleware/LogIncomingRequest';
import {router} from './middleware/Router';

export default async function main(): Promise<void> {

	await Database.initialise();

	const app = express();
	app.use(express.json());
	app.use(logIncomingRequest);
	app.use('/', router);
	app.use(gracefullyHandleErrors);

	app.listen(PORT);
	console.log(`listening on port ${PORT}`);

	// await Database.cards.insertMany([
	// 	{a : 1}, {a : 2}, {a : 3},
	// ]);
	// console.log('Inserted 3 documents into the collection');
}
