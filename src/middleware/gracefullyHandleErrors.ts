import express from 'express';
import {sendResponse} from './util/SendResponse';

export function gracefullyHandleErrors(err: Error, req: express.Request, res: express.Response, next: express.NextFunction): void  {
	if (res.headersSent) {
		next(err);
	} else {
		try {
			err = err || new Error('Server Error.');
			const messageString = err.toString();
			sendResponse(req, res, messageString, 500);
		} catch (secondError) {
			next(secondError);
		}
	}
}
