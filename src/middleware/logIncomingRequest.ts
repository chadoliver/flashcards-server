import express from 'express';

export function logIncomingRequest(req: express.Request, res: express.Response, next: express.NextFunction) {
	console.log('\nReceived request for:', req.path);
	next();
}
