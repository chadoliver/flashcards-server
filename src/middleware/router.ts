import express from 'express';
import _ from 'lodash';

import {deleteAllCards} from '../endpoints/deleteAllCards';
import {getAllCards} from '../endpoints/getAllCards';
import {getCardById} from '../endpoints/getCardById';
import {getNextCard} from '../endpoints/getNextCard';
import {postCardById} from '../endpoints/postCardById';
import {sendResponse} from './util/SendResponse';

export type HandlerFunction<T> = (req: express.Request, res: express.Response) => T;

function handleRequest<T>(func: HandlerFunction<T>) {
	return async (req: express.Request, res: express.Response, next: express.NextFunction): Promise<void> => {
		try {
			const message: T = await func(req, res);
			const text = _.isString(message) ? message : JSON.stringify(message);
			sendResponse(req, res, text, 200);
			next();
		} catch (error) {
			next(error);
		}
	};
}

export const router = express.Router();

router.get('/', handleRequest(() => 'hello world'));

router.get('/cards', handleRequest(getAllCards));
router.delete('/cards', handleRequest(deleteAllCards));
router.get('/cards/next', handleRequest(getNextCard));
router.get('/card/:id', handleRequest(getCardById));
router.post('/card/:id', handleRequest(postCardById));

// these are convenient GET methods, intended for development only
router.get('/delete-all-cards', handleRequest(deleteAllCards));
