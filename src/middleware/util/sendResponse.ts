import express from 'express';
import _ from 'lodash';

export function sendResponse(req: express.Request, res: express.Response, message: string, statusCode: number = 200): void {
	res.statusCode = res.statusCode || statusCode;
	res.set({
		'content-type': 'application/json; charset=utf-8',
		'content-length': _.size(message).toString(),
		'Access-Control-Allow-Origin': '*',
		'Access-Control-Allow-Credentials': 'true',
		'Access-Control-Allow-Methods': 'GET, POST, OPTIONS',
		'Access-Control-Allow-Headers': 'DNT,X-CustomHeader,Keep-Alive,User-Agent,X-Requested-With,If-Modified-Since,Cache-Control,Content-Type',
	});
	console.log('Sending response:', message);
	res.send(message);
}
